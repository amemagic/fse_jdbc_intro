import java.sql.*;

public class JdbcDemo {

    public static void main(String[] args) {
        System.out.println("JDBC Demo!");
        selectAllDemo();
        updateStudentDemo("Neuer Name", "neuemail@provider.at", 4);
        selectAllDemo();
        findAllByNameLike("Land");
        deleteStudentDemo(5);
        //INSERT INTO `student` (`id`, `name`, `email`) VALUES (NULL, 'Claudio Landerer', 'claudio.landerer@myimst.at'), (NULL, 'Maria Könne', 'maria.koenne@myimst.at');
    }

    private static void findAllByNameLike(String pattern) {
        System.out.println("Find all by name DEMO mit JDCB");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String pwd = "";

        try(Connection conn = DriverManager.getConnection(connectionUrl, user, pwd))
        {
            System.out.println("Verbindung zur DB hergestellt!");

            PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM `student` WHERE 'student'.'id' LIKE ?");
            preparedStatement.setString(1, "%"+pattern+"%");
            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next())
            {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                String email = rs.getString(3);
                System.out.println("Student aus der DB: [ID] " + id + " [NAME] " + name + " [EMAIL] " + email);
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verbindung zur DB: " + e.getMessage());
        }

    }


    public static void deleteStudentDemo(int studentID)
    {
        System.out.println("Delete DEMO mit JDCB");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String pwd = "";

        try(Connection conn = DriverManager.getConnection(connectionUrl, user, pwd))
        {
            System.out.println("Verbindung zur DB hergestellt!");
            PreparedStatement preparedStatement = conn.prepareStatement(
                    "DELETE FROM 'student' WHERE 'student'.'id' = ?"
            );
            try
            {
                preparedStatement.setInt(1,studentID);
                int rowAffected = preparedStatement.executeUpdate();
                System.out.println("Anzahl der gelöschten Datensätze: " + rowAffected);
            } catch(SQLException ex)
            {
                System.out.println("Fehler beim SQL-Delete-Statement: " + ex.getMessage());
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verbindung zur DB: " + e.getMessage());
        }
    }

    public static void updateStudentDemo(String neuerName, String neueEmail, int id)
    {
        System.out.println("Update DEMO mit JDCB");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String pwd = "";

        try(Connection conn = DriverManager.getConnection(connectionUrl, user, pwd))
        {
            System.out.println("Verbindung zur DB hergestellt!");
            PreparedStatement preparedStatement = conn.prepareStatement(
                    "UPDATE `student` SET 'name' = ?, `email` = ? WHERE `student`.`id` = ?"
            );
            try
            {
                preparedStatement.setString(1, neuerName);
                preparedStatement.setString(2, neueEmail);
                preparedStatement.setInt(3, id);
                int rowAffected = preparedStatement.executeUpdate();
                System.out.println(rowAffected + "Datensätze aktualisiert.");
            } catch(SQLException ex)
            {
                System.out.println("Fehler beim SQL-Update-Statement: " + ex.getMessage());
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verbindung zur DB: " + e.getMessage());
        }
    }

    public static void insertStudentDemo(String name, String email)
    {
        System.out.println("Insert DEMO mit JDCB");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String pwd = "";

        try(Connection conn = DriverManager.getConnection(connectionUrl, user, pwd))
        {
            System.out.println("Verbindung zur DB hergestellt!");
            PreparedStatement preparedStatement = conn.prepareStatement(
                    "INSERT INTO `student` (`id`, `name`, `email`) VALUES (NULL, ?, ?)"
            );
            try
            {
                preparedStatement.setString(1, name);
                preparedStatement.setString(2, email);
                int rowAffected = preparedStatement.executeUpdate();
                System.out.println(rowAffected + "Datensätze eingefügt.");
            } catch(SQLException ex)
            {
                System.out.println("Fehler im SQL-Insert-Statement: " + ex.getMessage());
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verbindung zur DB: " + e.getMessage());
        }
    }

    public static void selectAllDemo()
    {
        System.out.println("Select DEMO mit JDCB");
        String connectionUrl = "jdbc:mysql://localhost:3306/jdbcdemo";
        String user = "root";
        String pwd = "";

        try(Connection conn = DriverManager.getConnection(connectionUrl, user, pwd))
        {
            System.out.println("Verbindung zur DB hergestellt!");

            PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM `student`");
            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next())
            {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                String email = rs.getString(3);
                System.out.println("Student aus der DB: [ID] " + id + " [NAME] " + name + " [EMAIL] " + email);
            }
        } catch (SQLException e) {
            System.out.println("Fehler beim Aufbau der Verbindung zur DB: " + e.getMessage());
        }

    }

}
